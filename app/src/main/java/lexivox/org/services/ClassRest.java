package lexivox.org.services;



import android.content.Context;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;

/**
 * Created by root on 7/19/16.
 */
public abstract class ClassRest extends AsyncHttpResponseHandler {
    protected Context context;
    private boolean finalizado;
    private boolean exitoso;

    public ClassRest(Context context) {
        this.context = context;
    }

    public boolean isExitoso() {
        return exitoso;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    @Override
    public void onSuccess(int i, Header[] headers, byte[] bytes) {
        exitoso = true;
    }

    protected abstract String getURL();

    protected abstract String getMetod();

    protected abstract void onErrorLogico(String error);

    protected void callToGetWithutParameterMetods(
            AsyncHttpResponseHandler handler, RequestParams params ) {
        RestClient.allowCircularRedirects();
        RestClient.setTimeOut();
        RestClient.get(getURL(),getMetod(),params,handler);
    }


    @Override
    public void onFinish() {
        finalizado = true;
        super.onFinish();
    }
}