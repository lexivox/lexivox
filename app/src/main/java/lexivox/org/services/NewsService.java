package lexivox.org.services;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import lexivox.org.models.NormaInfo;

/**
 * Created by root on 8/4/16.
 */
public abstract class NewsService extends  ClassRest{
    private List<NormaInfo> normInfoList;

    public NewsService(Context context) {
        super(context);

    }

    @Override
    protected String getURL() {
        return "http://www.lexivox.org/public/own_rss/";
    }

    @Override
    protected String getMetod() {
        return "novedades_lexivox.xml";
    }


    @Override
    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
        Log.d("ERROR", throwable.getMessage());
    }

    public void postSearchNormInfo(String idtiponorm,  String nronorm, String searchword, String idpais,
                                   String estado, String conjurisprudencia, String dcmiidentifier,
                                   String fechamin, String fechamax) {
        RequestParams params = new RequestParams();
        params.add("id_tipo_norma", idtiponorm);
        params.add("nro_norma", nronorm);
        params.add("search_xml", searchword);
        params.add("id_pais", idpais);
        params.add("estado",estado);
        params.add("con_jurisprudencia", conjurisprudencia);
        params.add("dcmi_identifier", dcmiidentifier);
        params.add("fecha_promulgacion_min", fechamin);
        params.add("fecha_promulgacion_max", fechamax);
        callToGetWithutParameterMetods(this , params);
    }

    @Override
    public void onSuccess(int i, Header[] headers, byte[] bytes) {
        try {
            normInfoList = new ArrayList<>();
            String  result  =  new String(bytes);
            JSONObject jsonObject = new JSONObject(result);
            if(jsonObject.has("norm")){
                String jsonObjectNorm = jsonObject.getString("norm");
                JSONArray jsonArray =  new JSONArray(jsonObjectNorm);
                onSuccessObtenerNorms(jsonArray);
            }else{
                Log.d("NORMAS", "NO HAY RESULTADOS");
                String message = "No hay resultado para esta busqueda";
                onSuccessNotResult(message);
            }


        } catch (Exception e) {
            Log.d("no consume normas", e.getMessage());
        }
    }



    @Override
    protected void onErrorLogico(String error) {

    }

    public abstract void onSuccessObtenerNorms(JSONArray normInfoList);
    public  abstract void onSuccessNotResult(String message);


}