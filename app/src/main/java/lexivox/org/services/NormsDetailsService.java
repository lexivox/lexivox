package lexivox.org.services;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by root on 7/25/16.
 */
public abstract class NormsDetailsService  extends  ClassRest{
        private  String name="";
        private  String description;

    public NormsDetailsService(Context context) {
        super(context);
    }


    @Override
    protected String getURL() {
        return "http://www.lexivox.org/norms/";
    }

    @Override
    protected String getMetod() {
        return name;
    }


    @Override
    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
        Log.d("ERROR", throwable.getMessage());
    }

    public void postSearchNormInfoDetails(String name) {

        this.name=name+".nhtml";
        callToGetWithutParameterMetods(this , null);
    }

    @Override
    public void onSuccess(int i, Header[] headers, byte[] bytes) {
        try {
            description = new String(bytes, "UTF-8");
            Log.d("DATOS",description);
        } catch (Exception e) {
            Log.d("no consume normas", e.getMessage());
        }
        onSuccessObtenerNormsDetails(description);
    }



    @Override
    protected void onErrorLogico(String error) {

    }

    public abstract void onSuccessObtenerNormsDetails(String description);


}
