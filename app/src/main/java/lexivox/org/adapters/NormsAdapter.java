package lexivox.org.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import lexivox.org.activity.LexivoxAplication;
import lexivox.org.activity.R;
import lexivox.org.models.DaoSession;
import lexivox.org.models.NormaInfo;
import lexivox.org.models.NormaInfoDao;

/**
 * Created by root on 7/12/16.
 */
public class NormsAdapter  extends RecyclerView.Adapter<NormsAdapter.NormViewHolder>  {
    private Context context;
    private List<NormaInfo> normList;
    public  static OnItemClickListener myClickListener;
    public NormsAdapter(Context context, List<NormaInfo> normList) {
        this.normList = normList;
        this.context=context;
    }

    @Override
    public int getItemCount() {
        return normList.size();
    }

    @Override
    public void onBindViewHolder(final NormViewHolder normtViewHolder, int i) {
        final NormaInfo ci = normList.get(i);
        normtViewHolder.vNormTitle.setText(ci.getDcmititle());
        normtViewHolder.vNormContent.setText(ci.getDcmidescripction());
        normtViewHolder.vNormSimbol.setText(ci.getSimbolo());
        if (ci.getFavorito()){
            normtViewHolder.vNormFavorite.setImageResource(R.mipmap.ic_action_star_10_yellow);
        }else{
            normtViewHolder.vNormFavorite.setImageResource(R.mipmap.ic_action_star_0);
        }
        normtViewHolder.vNormFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ci.getFavorito()){
                    normtViewHolder.vNormFavorite.setImageResource(R.mipmap.ic_action_star_0);
                    ci.setFavorito(false);
//                    DaoSession sesion = LexivoxAplication.getInstance().getDaoSession();
//                    NormaInfoDao  ndao = sesion.getNormaInfoDao();
//                    NormaInfo norma = ci;
//                    ndao.deleteInTx(norma);
                }else{
                    normtViewHolder.vNormFavorite.setImageResource(R.mipmap.ic_action_star_10_yellow);
                    ci.setFavorito(true);
//                    DaoSession sesion = LexivoxAplication.getInstance().getDaoSession();
//                    NormaInfoDao  ndao = sesion.getNormaInfoDao();
//                    NormaInfo norma = ci;
//                    ndao.insertOrReplace(norma);


                }

            }
        });

    }

    @Override
    public NormViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_layout_norm, viewGroup, false);

        return new NormViewHolder(itemView);
    }
    public void setOnItemCLickListener( final OnItemClickListener itemclick){
        this.myClickListener = itemclick;
    }
    

    public static class NormViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        protected TextView vNormTitle;
        protected TextView vNormContent;
        protected Button vNormSimbol;
        protected ImageView vNormFavorite;


        public NormViewHolder(View v) {
            super(v);
            vNormTitle =  (TextView) v.findViewById(R.id.txtTitleNorm);
            vNormContent = (TextView)  v.findViewById(R.id.txtContentNorm);
            vNormFavorite = (ImageView) v.findViewById(R.id.img_favorite_norm);
            vNormSimbol = (Button) v.findViewById(R.id.btnNormSimbol);
             v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(myClickListener!=null){
                myClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }
}