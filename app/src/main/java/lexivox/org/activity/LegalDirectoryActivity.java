package lexivox.org.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Spinner;

public class LegalDirectoryActivity extends AppCompatActivity {
    private Spinner spnDepartamento;
    private Spinner spnspecialty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_directory);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_chevron_left_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),android.R.drawable.ic_menu_close_clear_cancel);
        toolbar.setOverflowIcon(drawable);
        spnDepartamento =(Spinner)findViewById(R.id.spn_legal_directory_departamento);
        spnspecialty =(Spinner)findViewById(R.id.spn_legal_directory_specialty);
    }

}
