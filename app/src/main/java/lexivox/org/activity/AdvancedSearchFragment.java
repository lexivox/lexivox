package lexivox.org.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class AdvancedSearchFragment extends Fragment {
    private Spinner spiTypeNorm ;
    private EditText edtNroOfNorm;
    private EditText edtDateInit;
    private EditText edtDateEnd;
    private EditText edtWord;
    private DatePickerDialog from;
    private DatePickerDialog to;
    private SimpleDateFormat dateFormat;
    private Button btnSearchAdvanced;
    private   ArrayAdapter typeDataAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.advanced_layout,null) ;
        spiTypeNorm = (Spinner)v.findViewById(R.id.spi_type_norms_advanced_search);
        edtNroOfNorm = (EditText)v.findViewById(R.id.edt_nro_of_norm_advanced_search);
        edtDateInit = (EditText)v.findViewById(R.id.edt_date_init_advanced_search);
        edtDateEnd = (EditText)v.findViewById(R.id.edt_date_end_advanced_search);
        edtWord = (EditText)v.findViewById(R.id.edt_word_advanced_search);
        edtDateEnd.setInputType(InputType.TYPE_NULL);
        edtNroOfNorm.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
        edtDateInit.setInputType(InputType.TYPE_NULL);
        btnSearchAdvanced = (Button)v.findViewById(R.id.btn_search_advanced);
        typeDataAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.type_arrays, R.layout.item_type_norm);
        spiTypeNorm.setAdapter(typeDataAdapter);
        return v ;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Calendar newDate = Calendar.getInstance();
        edtDateInit.setText( String.valueOf(newDate.getTime().getDate()+"/"+newDate.getTime().getMonth()+"/"+newDate.getTime().getYear()));
        edtDateEnd.setText( String.valueOf(newDate.getTime().getDate()+"/"+newDate.getTime().getMonth()+"/"+newDate.getTime().getYear()));
        edtDateInit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();
                from = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        edtDateInit.setText(dayOfMonth+"/"+monthOfYear+"/"+year);

                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                from.show();
            }
        });
       edtDateEnd.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Calendar newCalendar = Calendar.getInstance();
               to = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                   public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                       Calendar newDate = Calendar.getInstance();
                       newDate.set(year, monthOfYear, dayOfMonth);
                       edtDateEnd.setText(dayOfMonth+"/"+monthOfYear+"/"+year);

                   }

               },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
               to.show();
           }
       });
        btnSearchAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateFields()){
                    Intent intViewNorms = new Intent(getActivity(), NormsActivity.class);
                    intViewNorms.putExtra("searchWord", edtWord.getText().toString());
                    intViewNorms.putExtra("tipo", spiTypeNorm.getSelectedItem().toString());
                    intViewNorms.putExtra("nronorma", edtNroOfNorm.getText().toString());
                    intViewNorms.putExtra("datefrom", edtDateInit.getText().toString());
                    intViewNorms.putExtra("dateto",edtDateEnd.getText().toString());
                    startActivity(intViewNorms);
                }
            }
        });
    }
    private  boolean validateFields(){
        boolean validate=true;

//        if (edtNroOfNorm.getText().length()==0){
//            edtNroOfNorm.setError("Ingrese un Nro. de norma");
//            validate=false;
//        }
        if (edtWord.getText().length()==0){
            edtWord.setError("Ingrese texto a buscar");
            validate=false;
        }

        return  validate;
    }
}
