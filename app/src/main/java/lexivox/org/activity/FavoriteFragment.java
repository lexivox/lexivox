package lexivox.org.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import lexivox.org.adapters.NormsAdapter;
import lexivox.org.models.DaoSession;
import lexivox.org.models.NormaInfo;

public class FavoriteFragment extends Fragment {

    private RecyclerView recList;
    private RecyclerView.Adapter recAdapter;
    private RecyclerView.LayoutManager recLayoutManager;
    private NormsAdapter ca;
    private List<NormaInfo> result = new ArrayList<>();
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v =  inflater.inflate(R.layout.favorite_layout,null);
            recList = (RecyclerView)v.findViewById(R.id.cardList);
            recList.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);
            ca = new NormsAdapter(getActivity(),readlist());
            recList.setAdapter(ca);
            return  v;
        }
        private List<NormaInfo> readlist() {
            List<NormaInfo> result = new ArrayList<>();
            DaoSession sesion = LexivoxAplication.getInstance().getDaoSession();
            result = sesion.getNormaInfoDao().loadAll();
            return result;
        }

    }
