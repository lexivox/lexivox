package lexivox.org.activity;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import org.greenrobot.greendao.database.Database;

import lexivox.org.models.DaoMaster;
import lexivox.org.models.DaoSession;


/**
 * Created by root on 7/18/16.
 */
public class LexivoxAplication  extends Application{
    public static LexivoxAplication instance;
    private DaoSession daoSession;
    public static final boolean ENCRYPTED = true;


    public static LexivoxAplication getInstance(){
        return  instance;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "lexivox-db");
        Database db =  helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    public static  void saveUserShareEmail(String email){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getInstance());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Email",email);
        editor.apply();
    }

    public  static String  getUserShareEmail(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getInstance());
        return settings.getString("Email", "");
    }



}
