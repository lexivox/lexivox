package lexivox.org.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lexivox.org.util.Validate;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText edtPasswordForgot;
    private Button btnSendForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        edtPasswordForgot= (EditText)findViewById(R.id.edt_password_forgot);
        btnSendForgot = (Button)findViewById(R.id.btn_send_forgot);
        btnSendForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField()){
                    Toast.makeText(getApplicationContext(), "Enviado, rebice su correo", Toast.LENGTH_LONG ).show();
                }else{
                    edtPasswordForgot.setError("Ingrese un correo valido");
                }
            }
        });

    }

    private boolean validateField(){
        boolean estado=true;
        String email = edtPasswordForgot.getText().toString().trim();
        if (!Validate.emailValidator(email)){
            edtPasswordForgot.setError("Ingrese un correo valido");
            estado=false;
        }
        return  estado;
    }

}
