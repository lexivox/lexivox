package lexivox.org.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import lexivox.org.adapters.NormsAdapter;
import lexivox.org.models.NormaInfo;

public class NewFragment extends Fragment {
    RecyclerView recList;
    NormsAdapter ca;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.news_layout,null);
        recList = (RecyclerView)v.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        ca = new NormsAdapter(getActivity(),createList(5));
        recList.setAdapter(ca);
        return  v;
    }
    private List<NormaInfo> createList(int size) {

        List<NormaInfo> result = new ArrayList<>();

//        for (int i=1; i <= size; i++) {
//            NormInfo ci = new NormInfo();
//            ci.title = "Ley - 19390524 " ;
//            ci.content = "Trabajo, 24 de mayo de 1939 Ley General del Trabajo.-- Pónese en vigencia a partir...";
//            ci.symbol = "L";
//            ci.favorite = R.mipmap.ic_action_star_0;
//            result.add(ci);
//        }

        return result;
    }

}
