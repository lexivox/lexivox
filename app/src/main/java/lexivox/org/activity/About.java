package lexivox.org.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

public class About extends AppCompatActivity {
    private ImageView imgDevent;
    private ImageView imgLexivox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_chevron_left_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgDevent = (ImageView)findViewById(R.id.img_devenet);
        imgLexivox = (ImageView)findViewById(R.id.img_lexivox);
        imgLexivox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPage("http://www.lexivox.org/");
            }
        });
        imgDevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPage("http://www.devenet.net/?lang=es");
            }
        });

    }

    private void goPage(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }


}
