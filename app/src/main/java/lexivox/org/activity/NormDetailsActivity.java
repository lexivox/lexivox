package lexivox.org.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import cz.msebera.android.httpclient.Header;
import lexivox.org.models.NormaInfo;
import lexivox.org.services.NormsDetailsService;
import lexivox.org.services.NormsService;


public class NormDetailsActivity extends AppCompatActivity {
    private NormsDetailsService norms;
    private WebView txtContentNormDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Gson gS = new Gson();
        String target = getIntent().getStringExtra("norm");
        final NormaInfo normObject = gS.fromJson(target, NormaInfo.class);
        setContentView(R.layout.activity_norm_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_chevron_left_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.mipmap.ic_action_setting);
        toolbar.setOverflowIcon(drawable);
        toolbar.inflateMenu(R.menu.norms_details);
        TextView title  = (TextView)toolbar.findViewById(R.id.toolbar_title_norm_details);
        title.setText((normObject.getDcmiidentifier()));
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id){
                    case R.id.share:
                        Intent intent=new Intent(android.content.Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Lexivox");
                        intent.putExtra(Intent.EXTRA_TEXT, normObject.getDcmititle()+": "+"http://www.lexivox.org/norms/"+normObject.getDcmiidentifier()+".xhtml");
                        startActivity(intent);
                    case R.id.download:

                    case  R.id.search:

                }
                return true;
            }
        });


        txtContentNormDetails = (WebView)findViewById(R.id.txt_norm_details_content);
        loadNorm(normObject.getDcmiidentifier());
    }

    @Override
    protected void onResume() {
        super.onResume();

        txtContentNormDetails.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                txtContentNormDetails.loadUrl(url);

                return  true;
            }
        });

    }
    private void loadNorm(String name){


        norms = new NormsDetailsService(this) {
            @Override
            public void onSuccessObtenerNormsDetails( String description) {
                Log.d("correcto",description);
                txtContentNormDetails.loadDataWithBaseURL("",description,"text/html","UTF-8", "");
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                super.onFailure(i, headers, bytes, throwable);
                Log.d("error", throwable.getMessage());
            }
        };
        norms.postSearchNormInfoDetails(name);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
   //     getMenuInflater().inflate(R.menu.norms_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }
}
