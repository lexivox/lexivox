package lexivox.org.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import lexivox.org.models.DaoSession;
import lexivox.org.models.User;
import lexivox.org.models.UserDao;
import lexivox.org.util.Validate;


public class Login extends AppCompatActivity {
    private Button btnLogin;
    private TextView txtSignUp ;
    private TextView txtForgotPassword;
    private EditText edtEmail;
    private EditText edtPassowrd;
    private User userLexivox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtSignUp = (TextView)findViewById(R.id.txt_sign_up);
        txtForgotPassword = (TextView)findViewById(R.id.txt_forgot_password);
        edtEmail = (EditText)findViewById(R.id.edt_email_login);
        edtPassowrd = (EditText)findViewById(R.id.edt_password_login);
        btnLogin = (Button)findViewById(R.id.btn_login);
        userLexivox = new User();
        beginApp();
        beginAccount();
        beginForgotAccount();
    }

    private void beginApp(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField()){
                    userLexivox.setEmail(edtEmail.getText().toString());
                    userLexivox.setPassword(edtPassowrd.getText().toString());
                    DaoSession sesion = LexivoxAplication.getInstance().getDaoSession();
                    List<User> users = sesion.getUserDao().queryBuilder().where(UserDao.Properties.Email.eq(userLexivox.getEmail()),UserDao.Properties.Password.eq(userLexivox.getPassword())).list();
                    if (users.size()>0){
                        Log.d("user",users.get(0).getEmail());
                            LexivoxAplication.saveUserShareEmail(users.get(0).getEmail());
                            Intent i = new Intent(Login.this, MainActivity.class);
                            overridePendingTransition(R.anim.enter_from_left,R.anim.stay_still);
                            startActivity(i);
                            finish();
                    }else{
                            final Dialog dialog = new Dialog(Login.this);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setContentView(R.layout.dialog_error);
                            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                            dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }
                }else{

                }
            }
        });
    }
    private  void beginAccount(){

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, AccountActivity.class);
                overridePendingTransition(R.anim.enter_from_left,R.anim.stay_still);
                startActivity(i);
            }
        });
    }
    private  void beginForgotAccount(){
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, ForgotPasswordActivity.class);
                overridePendingTransition(R.anim.enter_from_left,R.anim.stay_still);
                startActivity(i);

            }
        });

    }

    private boolean validateField(){
        boolean estado=true;
        String email = edtEmail.getText().toString().trim();
        if (!Validate.emailValidator(email)){
            edtEmail.setError("Ingrese un correo válido");
            estado=false;
        }
        if (edtPassowrd.getText().length()==0){
            edtPassowrd.setError("Ingresa una contraseña válida");
            estado=false;
        }
        return  estado;
    }

}
