package lexivox.org.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class Inicio extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        final boolean b = new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String email = LexivoxAplication.getInstance().getUserShareEmail();
                if ( !email.equals("")) {

                    Intent i = new Intent(Inicio.this, MainActivity.class);
                    overridePendingTransition(R.anim.enter_from_left, R.anim.stay_still);
                    startActivity(i);
                    finish();
                }else{
                    Intent i = new Intent(Inicio.this, Login.class);
                    overridePendingTransition(R.anim.enter_from_left, R.anim.stay_still);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
