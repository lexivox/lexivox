package lexivox.org.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class SearchFragment extends Fragment {

    private EditText txtSearchText;
    private Button btnSearch;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.search_layout,null);
        btnSearch = (Button)view.findViewById(R.id.btn_search);
        txtSearchText = (EditText)view.findViewById(R.id.txt_search_text);
        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadSearchNorms();
    }

    private void loadSearchNorms(){
            btnSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (txtSearchText.getText().toString().length()!=0){
                        Intent initSearch =  new Intent(getActivity(), NormsActivity.class );
                        initSearch.putExtra("searchWord", txtSearchText.getText().toString());
                        startActivity(initSearch);
                    }else{
                        txtSearchText.setError("Ingrese texto a buscar");
                    }
                }
            });
    }
}
