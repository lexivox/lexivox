package lexivox.org.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Console;

import lexivox.org.models.DaoSession;
import lexivox.org.models.User;
import lexivox.org.util.PasswordValidator;
import lexivox.org.util.Validate;


public class AccountActivity extends AppCompatActivity {
    private Button btnAccountRegister;
    private EditText edtAccountEmail;
    private EditText edtAcoountPassword;
    private User userLexivox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userLexivox = new User();
        setContentView(R.layout.activity_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnAccountRegister = (Button)findViewById(R.id.btn_account_register);
        edtAccountEmail = (EditText) findViewById(R.id.edt_email);
        edtAcoountPassword = (EditText)findViewById(R.id.edt_password);
        AccountRegister();
    }


    private void AccountRegister(){
        btnAccountRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if( !validateFields()){
                   userLexivox.setEmail(edtAccountEmail.getText().toString());
                   userLexivox.setPassword(edtAcoountPassword.getText().toString());
                   DaoSession sesion = LexivoxAplication.getInstance().getDaoSession();
                   Long result  = sesion.getUserDao().insert(userLexivox);

               }
            }
        });
//        edtAccountEmail.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                txtErrEmail.setVisibility(View.GONE);
//                return false;
//            }
//        });
//        edtAcoountPassword.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                txtErrPassword.setVisibility(View.GONE);
//                return false;
//            }
//        });

    }

    private boolean validateFields(){
        PasswordValidator p = new PasswordValidator();
        if (Validate.isValidEmail(edtAccountEmail.getText()) && p.validate(edtAcoountPassword.getText().toString())  ){
            return  true;
        }else{
            if (!Validate.isValidEmail(edtAccountEmail.getText()) ){
                edtAccountEmail.setError("Ingrese un correo válido");
            }
            if (!p.validate(edtAcoountPassword.getText().toString()) ){
                edtAcoountPassword.setError("Ingrese una contraseña válida");
            }
            return  false;
        }


    }

}
