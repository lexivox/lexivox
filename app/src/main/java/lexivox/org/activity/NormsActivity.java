package lexivox.org.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import android.widget.TextView;

import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import cz.msebera.android.httpclient.Header;
import lexivox.org.adapters.NormsAdapter;
import lexivox.org.models.NormaInfo;
import lexivox.org.services.NormsService;

public class NormsActivity extends AppCompatActivity {
    private RecyclerView recList;
    private NormsAdapter normAdapter;
    private TextView txtError;
    private List<NormaInfo> result = new ArrayList<>();
    private NormsService norms;
    private String searchWord;
    private String tipo;
    private String nroNorma;
    private String dateFrom;
    private String dateTo;
    private CircularProgressBar circularProgressBar;
    int pStatus = 0;
    private Timer timer;
    private TextView txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        searchWord = bundle.getString("searchWord");
        if(bundle.containsKey("tipo")){
            this.tipo=bundle.getString("tipo");
        }
        if(bundle.containsKey("nronorma")){
            this.nroNorma=bundle.getString("nronorma");
        }
        if(bundle.containsKey("datefrom")){
            this.dateFrom=bundle.getString("datefrom");

        }
        if(bundle.containsKey("dateto")){
            this.dateTo=bundle.getString("dateto");
        }
        setContentView(R.layout.activity_norms);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_chevron_left_white_36dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),android.R.drawable.ic_menu_close_clear_cancel);
        toolbar.setOverflowIcon(drawable);
        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        getSearchNormsList(searchWord);
        txtError = (TextView)findViewById(R.id.txt_error_search);
        circularProgressBar = (CircularProgressBar)findViewById(R.id.progress_norm);
        txtMessage = (TextView)findViewById(R.id.txt_message_error_and_load);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        circularProgressBar.setProgress(circularProgressBar.getProgress() + 1);

                    }
                });
            }
        }, 1000, 100);
    }

    private void getSearchNormsList(String searchWord){
        norms = new NormsService(this) {
            @Override
            public void onSuccessObtenerNorms(JSONArray normInfoList) {

                for (int i = 0; i < normInfoList.length() ; i++) {
                    try {
                        NormaInfo norm = new NormaInfo(normInfoList.getJSONObject(i));
                        result.add(norm);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                normAdapter = new NormsAdapter(getBaseContext(),result);
                recList.setAdapter(normAdapter);
                normAdapter.setOnItemCLickListener(new NormsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        NormaInfo norm = new NormaInfo();
                        norm = result.get(position);
                        Gson gS = new Gson();
                        String target = gS.toJson(norm);
                        Intent iNormDetails = new Intent(getBaseContext(), NormDetailsActivity.class);
                        iNormDetails.putExtra("norm", target);
                        startActivity(iNormDetails);
                    }
                 });
            }

            @Override
            public void onSuccessNotResult(String message) {
                    txtError.setText(message);
                    txtError.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
               super.onFailure(i, headers, bytes, throwable);
                timer.cancel();
                circularProgressBar.setVisibility(View.GONE);
                circularProgressBar.clearAnimation();
                txtMessage.setText("Error de conexion, intentelo nuevamente. ");
            }
            @Override
            public void onFinish() {
                super.onFinish();
                timer.cancel();
                circularProgressBar.setVisibility(View.GONE);
                circularProgressBar.clearAnimation();
                txtMessage.setVisibility(View.GONE);
                recList.setVisibility(View.VISIBLE);
            }
        };
        if (tipo!=null || nroNorma!=null || dateFrom!=null||dateTo!=null ){
            norms.postSearchNormInfo(tipo,nroNorma, searchWord, "25","vigentes","f","",  "2015-06-22","2016-06-22");
        }else {
            norms.postSearchNormInfo("","", searchWord, "25","vigentes","f","","","");
        }
    }
}
